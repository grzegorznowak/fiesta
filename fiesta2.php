<?php
/*
* Tell me you are indexable without mentioning arrays
*/
function indexable($a, $v = null, $f = null)  {
        if($f == null) { $f = function() { return null; }; }
        $f2 = function($newA, $newV = null) use($a, $v, &$f2, $f) {
                if($newV !== null) {
                        return indexable($newA, $newV, $f2);
                }
                if($newA == $a) {
                        return $v;
                }
                return $f($newA);

        };
        return  $f2;
};

// abrakadabra
$b = indexable(0, 99);
$b = $b(1, 100);
$b = $b(2, 'hello array');
$b = $b(3, 101);

// indexable af
echo($b(0).PHP_EOL);
echo($b(1).PHP_EOL);
echo($b(2).PHP_EOL);
echo($b(3).PHP_EOL);
