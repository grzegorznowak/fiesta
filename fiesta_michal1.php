<?php

$pubkey = '3232142,1232131';
$text = 'textToEncrypt';

function encrypt($text, $pubkey)
{
	$text = str_split(str_replace(' ', '', $text));
	if (sizeof($text)%2 != 0) {
		array_push($text, '000');
	}
	$asciiArray = array();
	for ($i = 1; $i < count($text); $i = $i+2) {
		array_push($asciiArray, asciiPairs($text[$i], $text[$i+1]));
	}
	$encryptedText = '';
	$pubkey = explode(',', $pubkey);
	foreach ($asciiArray as $asciiElement) {
		$encryptedText = ','.encodeRSA($asciiElement, $pubkey[0], $pubkey[1]);
	}

	return ltrim($encryptedText, ',');
}

function asciiPairs($firstLetter, $secondLetter) {
	$firstLetter = str_pad(ord($firstLetter), 3, '0', STR_PAD_RIGHT);
	if ($secondLetter != '000') {
		$secondLetter = str_pad(ord($secondLetter), 3, '0', STR_PAD_RIGHT);
	}

	return $firstLetter.$secondLetter;
}

function encodeRSA($a, $w, $n) {
	$pot = $a;
	$wyn = 1;
	for($q = $w; $q > 0; $q /= 2)
	{
		if($q % 2) {
			$wyn = ($wyn * $pot) % $n;
		}
		$pot = ($pot * $pot) % $n;
	}

	return $wyn;
}